##!/usr/bin/python

import socket
import sys
import threading
import queue
import board
import busio
import digitalio
import time
import random
import numpy as np


receivedDataFromCCS = queue.Queue()
dataToSendToCCS = queue.Queue()

spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)

cs = digitalio.DigitalInOut(board.D5)
reset = digitalio.DigitalInOut(board.D6)
led= digitalio.DigitalInOut(board.D17)
led.direction = digitalio.Direction.OUTPUT
print(led.value)
led.value = True
print(led.value)

import adafruit_rfm69
rfm69 = adafruit_rfm69.RFM69(spi, cs, reset, 915.0)
x=0

# Passed data from local Rover modules to CCS
def PassLocalDataToCCS():
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    # Bind the socket to the port
    server_address = ('localhost', 4567)
    print ('starting up on %s port %s' % server_address)
    sock.bind(server_address)
    
    while True:
        print('Waiting for data from local modules')
        data, address = sock.recvfrom(3000)

        if data:
            # replace with line to pass data to CCS through datalink
            print (data)
            dataToSendToCCS.put(data)

# Pass data from datalink to Rover
def PassDataFromCCSToRover():
    # Create a UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    server_address = ('localhost', 4211)
    while True:
        # Send data
        item = receivedDataFromCCS.get()
        sent = sock.sendto(item, server_address)

def datalinkControl():
    print('Receive Data From CCS')
    
    while True:
        packet = rfm69.receive(keep_listening=False)
        # If no packet was received during the timeout then None is returned.
        if packet is None:
            # Packet has not been received
            print("Received nothing! Listening again...")
        else:
            # Received a packet!
            receivedDataFromCCS.put(packet)
            
        if not dataToSendToCCS.empty():    
            dataToSend = dataToSendToCCS.get(block=False)
            #dataToSend=b'\xb4gtesk\ntiomalSp`aaE\x10<'
            rfm69.send(dataToSend)
            print("Sent Sensor Value: ",dataToSend)

#  Main

print('Start thread 1')
toCCS=threading.Thread(target=PassLocalDataToCCS)
toCCS.start()
print('Start thread 2')
toRover=threading.Thread(target=PassDataFromCCSToRover)
toRover.start()
print('Start thread 3')
radioControl=threading.Thread(target=datalinkControl)
radioControl.start()


