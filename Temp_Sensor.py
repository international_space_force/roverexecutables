import smbus
import time
import logging
from pymavlink import mavutil

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] (%(threadName)-5s) %(message)s', )

# Get I2C bus
bus = smbus.SMBus(1)


def main_temp():
    central_exec_conn = mavutil.mavlink_connection('udpout:127.0.0.1:4205')

    while True:
        # MPL3115A2 address, 0x60(96)
        # Select control register, 0x26(38)
        # 0xB9(185) Active mode, OSR = 128, Altimeter mode
        bus.write_byte_data(0x60, 0x26, 0xB9)

        # MPL3115A2 address, 0x60(96)
        # Select data configuration register, 0x13(19)
        # 0x07(07) Data ready event enabled for altitude, pressure, temperature
        bus.write_byte_data(0x60, 0x13, 0x07)

        # MPL3115A2 address, 0x60(96)
        # Select control register, 0x26(38)
        # 0xB9(185) Active mode, OSR = 128, Altimeter mode
        bus.write_byte_data(0x60, 0x26, 0xB9)
        time.sleep(.5)

        # MPL3115A2 address, 0x60(96)
        # Read data back from 0x00(00), 6 bytes
        # status, tHeight MSB1, tHeight MSB, tHeight LSB, temp MSB, temp LSB
        data = bus.read_i2c_block_data(0x60, 0x00, 6)

        # Convert the data to 20-bits
        tHeight = ((data[1] * 65536) + (data[2] * 256) + (data[3] & 0xF0)) / 16
        temp = ((data[4] * 256) + (data[5] & 0xF0)) / 16
        altitude = tHeight / 16.0
        cTemp = temp / 16.0
        fTemp = cTemp * 1.8 + 32

        # MPL3115A2 address, 0x60(96)
        # Select control register, 0x26(38)
        # 0x39(57) Active mode, OSR = 128, Barometer mode
        bus.write_byte_data(0x60, 0x26, 0x39)
        time.sleep(.5)

        # MPL3115A2 address, 0x60(96)
        # Read data back from 0x00(00), 4 bytes
        # status, pres MSB1, pres MSB, pres LSB
        data = bus.read_i2c_block_data(0x60, 0x00, 4)

        # Convert the data to 20-bits
        pres = ((data[1] * 65536) + (data[2] * 256) + (data[3] & 0xF0)) / 16
        pressure = (pres / 4.0) / 1000.0

        # Output data to screen
        logging.debug("Pressure: %.2f kPa" % pressure)
        logging.debug("Altitude: %.2f m" % altitude)
        logging.debug("Temp(C) : %.2f C" % cTemp)
        logging.debug("Temp(F) : %.2f F" % fTemp)

        msg = central_exec_conn.mav.raw_pressre_send(0, int(pressure), int(altitude), 0, int(fTemp))
        # logging.debug("=" * 40)  # Print a separator line
