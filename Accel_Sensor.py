import time
import board
import busio
import adafruit_mma8451
import logging
from pymavlink import mavutil

logging.basicConfig(level=logging.DEBUG,
                    format='[%(levelname)s] (%(threadName)-5s) %(message)s', )

# Initialize I2C bus
i2c = busio.I2C(board.SCL, board.SDA)

# Initialize MMA8451 module
sensor = adafruit_mma8451.MMA8451(i2c)
sensor.data_rate = adafruit_mma8451.DATARATE_50HZ  # 50Hz


def main_accel():
    central_exec_conn = mavutil.mavlink_connection('udpout:127.0.0.1:4205')

    # Main loop to print the acceleration and orientation every second
    while True:
        x, y, z = sensor.acceleration
        logging.debug('Accel: x={0:0.3f}m/s^2 y={1:0.3f}m/s^2 z={2:0.3f}m/s^2'.format(x, y, z))
        orientation_value = 0
        orientation = sensor.orientation
        # Orientation is one of these values:
        #  - PL_PUF: Portrait, up, front
        #  - PL_PUB: Portrait, up, back
        #  - PL_PDF: Portrait, down, front
        #  - PL_PDB: Portrait, down, back
        #  - PL_LRF: Landscape, right, front
        #  - PL_LRB: Landscape, right, back
        #  - PL_LLF: Landscape, left, front
        #  - PL_LLB: Landscape, left, back
        # logging.debug('Orientation: ', end='')
        if orientation == adafruit_mma8451.PL_PUF:
            logging.debug('Portrait, up, front')
            orientation_value = 1
        elif orientation == adafruit_mma8451.PL_PUB:
            logging.debug('Portrait, up, back')
            orientation_value = 2
        elif orientation == adafruit_mma8451.PL_PDF:
            logging.debug('Portrait, down, front')
            orientation_value = 3
        elif orientation == adafruit_mma8451.PL_PDB:
            logging.debug('Portrait, down, back')
            orientation_value = 4
        elif orientation == adafruit_mma8451.PL_LRF:
            logging.debug('Landscape, right, front')
            orientation_value = 5
        elif orientation == adafruit_mma8451.PL_LRB:
            logging.debug('Landscape, right, back')
            orientation_value = 6
        elif orientation == adafruit_mma8451.PL_LLF:
            logging.debug('Landscape, left, front')
            orientation_value = 7
        elif orientation == adafruit_mma8451.PL_LLB:
            logging.debug('Landscape, left, back')
            orientation_value = 8
        central_exec_conn.mav.local_position_ned_send(orientation_value, float(x), float(y), float(z), 0.0, 0.0, 0.0)
        time.sleep(1.0)
