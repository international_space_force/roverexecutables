from pymavlink import mavutil
from pymavlink.dialects.v20 import common as mavlink2
from datetime import datetime
import threading
import RPi.GPIO as GPIO
import time
import os

connection = mavutil.mavlink_connection('udpout:127.0.0.1:4205', dialect='ardupilotmega', retries=3)

# GPIO setup
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

# setup gpio for echo & trig
echopin = [24, 21]
trigpin = [22, 20]

for j in range(2):
    GPIO.setup(trigpin[j], GPIO.OUT)
    GPIO.setup(echopin[j], GPIO.IN)


def heartbeat_send():
    connection.mav.heartbeat_send(mavutil.mavlink.MAV_TYPE_ONBOARD_CONTROLLER,
                                  mavutil.mavlink.MAV_AUTOPILOT_INVALID, 0, 0, 0)
    threading.Timer(5, heartbeat_send).start()


# Get reading from HC-SR04
def ping(echo, trig):
    GPIO.output(trig, False)
    # Allow module to settle
    time.sleep(0.5)
    # Send 10us pulse to trigger
    GPIO.output(trig, True)
    time.sleep(0.00001)
    GPIO.output(trig, False)

    pulse_start = time.time()
    pulse_end = time.time()

    # save StartTime
    while GPIO.input(echo) == 0:
        pulse_start = time.time()

    # save time of arrival
    while GPIO.input(echo) == 1:
        pulse_end = time.time()

    # time difference between start and arrival
    pulse_duration = pulse_end - pulse_start

    distance = pulse_duration * 17150

    distance = round(distance, 2)

    return distance


def output():
    results = str(datetime.now().strftime('%m-%d-%Y %H:%M:%S')) + ","
    for j in range(2):
        distance = ping(echopin[j], trigpin[j])

        if distance <= 60.96:
            dist_inches = round((distance * 0.39370), 2)
            print("sensor", j + 1, ": ", distance, "cm", "(" + str(round((distance * 0.39370), 2)), "inches)")
            results = results + str(distance) + " (" + str(dist_inches) + " inches)" + ","
            connection.mav.distance_sensor_send(0, 10, 40, int(distance),
                                                mavutil.mavlink.MAV_DISTANCE_SENSOR_UNKNOWN, 1, 0, 0)
    results = results + "\n"

    # write results data to file
    with open("/home/pi/Desktop/obstacleDetection/data_log.csv", "a") as file:
        if os.stat("/home/pi/Desktop/obstacleDetection/data_log.csv").st_size == 0:
            file.write("Time,Sensor1,Sensor2\n")
        file.write(results)
    print("settling...")
    threading.Timer(1, output).start()


print("press Ctrl+c to stop program ")
# end = time.time
output()
heartbeat_send()
