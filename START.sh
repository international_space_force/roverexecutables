#!/bin/bash

echo Starting Rover Executables
lxterminal -e ./romcommod
lxterminal -e ./cenexecmod
lxterminal -e ./bolt_cam
lxterminal -e ./motor_controller
lxterminal -e ./robot_arm
lxterminal -e python3 Sensor_Suite.py
lxterminal -e python3 objDetection_sensor.py
lxterminal -e python3 path_planning.py
lxterminal -e python3 path_planning_client.py
lxterminal -e python3 RoverPassthrough.py
