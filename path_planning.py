from math import sin, cos, sqrt, atan2, radians
from pymavlink import mavutil
from pymavlink.dialects.v20 import common as mavlink2
import adafruit_gps
import re
import threading

connection = mavutil.mavlink_connection('udpin:127.0.0.1:4201', dialect='ardupilotmega', retries=3)


def heartbeat_send():
    connection = mavutil.mavlink_connection('udpout:127.0.0.1:4201', dialect='ardupilotmega', retries=3)
    connection.mav.heartbeat_send(mavutil.mavlink.MAV_TYPE_ONBOARD_CONTROLLER,
                                  mavutil.mavlink.MAV_AUTOPILOT_INVALID, 0, 0, 0)
    threading.Timer(5, heartbeat_send).start()


def distance_remaining():
    uart = serial.Serial("/dev/ttyS0", baudrate=9600, timeout=10)
    gps = adafruit_gps.GPS(uart, debug=False)

    R = 6373.0

    lat1 = '{0:.6f}'.format(gps.latitude)
    lat1 = radians(float(lat1))

    lon1 = '{0:.6f}'.format(gps.longitude)
    lon1 = radians(float(lon1))

    with open("gps.log", "r") as f:
        for last_line in f:
            pass

    last_line.split()
    lat2, lon2 = last_line.split()

    lat2 = float(lat2)
    lon2 = float(lon2)

    lat2 = radians(lat2)
    lon2 = radians(lon2)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c
    meters = round(distance * 1000, 2)

    # meters =  gps_listener()
    if meters <= 5:
        # Mission_Item_Reached (46)
        connection = mavutil.mavlink_connection('udpout:127.0.0.1:4201', dialect='ardupilotmega', retries=3)
        connection.mav.mission_item_reached_send(1)
    else:
        # Landing_Target (149)
        # Command for distance remaining
        connection = mavutil.mavlink_connection('udpout:127.0.0.1:4201', dialect='ardupilotmega', retries=3)
        connection.mav.landing_target_send(0, 0, 0, 0, 0, meters, 0, 0, 0)
    threading.Timer(1.5, distance_remaining).start()


distance_remaining()
heartbeat_send()
