#import GPS_Sensor
import Accel_Sensor
import Temp_Sensor
from threading import Thread

threads = []

#gps_thread = Thread(name='GPS', target=GPS_Sensor.main_gps)
#threads.append(gps_thread)
#gps_thread.start()

temp_thread = Thread(name='Temp', target=Temp_Sensor.main_temp)
threads.append(temp_thread)
temp_thread.start()

accel_thread = Thread(name='Accel', target=Accel_Sensor.main_accel)
threads.append(accel_thread)
accel_thread.start()
